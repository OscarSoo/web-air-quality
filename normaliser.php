<?php

$loc = array(
3=> 'Brislington',
6=> 'Fishponds',
8=> 'Parson st',
9=> 'Rupert st',
10=> 'Wells road',
11=> 'Newfoundland way'
);

foreach ($loc as $key => $val)
{
    $file_name = str_replace(' ' , '_', $val);
    $file_name = "data/".strtolower($file_name).".xml";

    $xmlreader = new XMLReader();
    $xmlreader->open($file_name);
    
    $outer = array();
 
    while( $xmlreader->read() ) 
    {
            if($xmlreader->nodeType !== XMLReader::ELEMENT){
                continue;
            }
            if($xmlreader->localName === 'desc'){
                 $inner = array();
                 $inner['desc'] = $xmlreader->getAttribute('val');
            }
            if($xmlreader->localName === 'lat'){
                 $inner['lat'] = $xmlreader->getAttribute('val');
            }
            if($xmlreader->localName === 'long'){
                $inner['long'] = $xmlreader->getAttribute('val');
            }
            if($xmlreader->localName === 'date'){
                $inner['date'] = $xmlreader->getAttribute('val');
            }
            if($xmlreader->localName === 'time'){
                $inner['time'] = $xmlreader->getAttribute('val');
            }
            if($xmlreader->localName === 'no2'){
                $inner['no2'] = $xmlreader->getAttribute('val');
                $outer[] = $inner;
            }  
    }
    
    $xmlreader->close();

    $writer = new XMLWriter();
	$writer->openUri("normalized_data/".str_replace(' ' , '_', $val)."_no2.xml");

	$writer->setIndentString('  ');
	$writer->setIndent(true);

	$writer->startDocument( '1.0', 'UTF-8' );
	
    $writer->startElement('data');
    $writer->writeAttribute('type', 'nitrogen dioxide');

	$writer->startElement('location');
	$writer->writeAttribute('id', $val);
    $writer->writeAttribute('lat', $inner['lat']);
    $writer->writeAttribute('long', $inner['long']);
    
    foreach($outer as $inner)
    {
        $writer->startElement('reading');
        $writer->writeAttribute('date', $inner['date']);
        $writer->writeAttribute('time', $inner['time']);
        $writer->writeAttribute('val', $inner['no2']);
        $writer->endElement(); /* </reading> */
    }

	$writer->endElement();   /* </location> */
	$writer->endElement();	/* </data> */
	$writer->endDocument();
    $writer->flush();
}
    echo "Completed";
?>