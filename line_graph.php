<?php
//Array
$records = array();
$row = array();
$data = array();
$finalData = array();
$loc = array(3=> 'Brislington', 6=> 'Fishponds', 8=> 'Parson St',
             9=> 'Rupert St', 10=> 'Wells Road',11=> 'Newfoundland Way');

//Get location selected
    if (!empty($_GET['loc'])) {
            $selectedLoc = $_GET['loc'];
    } else {    
            $selectedLoc = "Brislington"; //select default if not
    }
//Get date selected
    if (!empty($_GET['date'])) {
        $date = $_GET['date'];
        $selectDate = date("d/m/Y", strtotime($date)); //change string to timestamp and change its format
    } else {    
        $selectDate = null; //if it's empty and selectDate's value is null and let user know to select date
        echo "Please select a date.";
    }

    $reader = new XMLReader();      //reader
    $filename = str_replace(' ', '_', $selectedLoc);    //replace empty spaces
    $file_name = "normalized_data/".strtolower($filename) . '_no2.xml';     //get file
	if(!$reader->open($file_name)){
		die("Failed to open!!!");
	}
    while ($reader->read()) {   //read the file and get time and no2 reading
        if ($reader->nodeType == XMLReader::ELEMENT && $reader->localName == 'reading') { 
            if ($reader->getAttribute('date') == $selectDate) {
                $row['Hour'] = substr($reader->getAttribute('time'), 0,2);   //get the 1st 2digits of the time to get hour and store into array
                $row['no2'] = $reader->getAttribute('val');     //get no2 value and store into array
                $records[] = $row;      //store both arrays into one array 
            }
        }
    }

function countAverage (&$records, $hour){   //function to get the average

    $count=null;//counter to count repeated hour 
    $countNo2=null;
    
    foreach ($records as $reading){
        
        if($reading['Hour'] == $hour){ //display value that has the same hour       
                $countNo2 += $reading['no2']; //add up the NO2 Value
                $count++;
        }
    }
    
    if($count !=0){//get the average of the NO2 Value
        $avgNo2 = $countNo2/$count;
    }else{//if no data in that hour
        $avgNo2 =0; //average of NO2 = zero
    }
    
    $formatAvg = number_format($avgNo2, 2, '.', '');//format NO2 Value to 2 decimal places
    $data['Hour']=$hour; //store each hour
    $data['avgNo2'] = $formatAvg; //store the average of NO2 Value 
    $finalData[] = $data; //store it into a big array
    
    foreach ($finalData as $r){//display the value ino the chart
        
        echo "['" . $r['Hour'] . "'," . $r['avgNo2']. "],";
    }
    
}
?>

<html>
    <head>
       <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
       <script type="text/javascript"> 
      google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'X');
          data.addColumn('number', '<?php echo $selectedLoc." on ". $selectDate ;?>');
            
            
          data.addRows([
            <?php 
            for($hour=0;$hour<24;$hour++)
            {
                countAverage($records, $hour);
            } 
            ?>
          ]);

          var options = {
            title: ' NO2 Value of <?php echo $selectedLoc; ?> on <?php echo $selectDate; ?> ',  
            hAxis: {title: 'Hours' ,minValue:0,maxValue:24},
            vAxis: {title: 'NO2 Concentration'},
                pointSize: 3,
                pointShape: 'circle'
            };

          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

          chart.draw(data, options);
        }
       </script>
    </head>
    <body>

 <form action ="<?php echo $_SERVER['PHP_SELF'];?>" method="GET">
     <p>Station:
        <select name="loc" method="get" onchange='this.form.submit()'>
        <option selected="selected">
        <?php
            echo $selectedLoc;
        ?>    
        </option>
        <?php
        foreach($loc as $value):
        echo '<option value="'.$value.'">'.$value.'</option>'; 
        endforeach;
        ?>
        </select>
         <br><br>
 
         Select a date:
        <input type="date" name="date" value="<?php echo $date?>" method="get" onchange='this.form.submit()' min="2014-11-13" max="2017-02-20">
  </form>
        
    <div id="chart_div" style="width: 900px; height: 500px"></div>
</body>
</html>