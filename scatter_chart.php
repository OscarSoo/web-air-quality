<?php
//Array
$loc = array(3=> 'Brislington', 6=> 'Fishponds', 8=> 'Parson St',
             9=> 'Rupert St', 10=> 'Wells Road',11=> 'Newfoundland Way');
$year = array('2015','2016','2017');
$records = array();
$row = array();
$found = '';

//If not selected it will set Brislington by default and stay the same after selected
if (!empty($_GET['Dropdown'])) {
        $dropDownVal = $_GET['Dropdown'];
    } else {
        $dropDownVal = "Brislington";   //select default if not
    }

if (!empty($_GET['Year'])) {
        $dropYearVal = $_GET['Year'];
    } else {
        $dropYearVal = "2017";      //select default if not
    }

//Read XML into array
    $file_name = str_replace(' ' , '_', $dropDownVal);
    $file_name = "normalized_data/".strtolower($file_name)."_no2.xml";

    $xmlreader = new XMLReader();   //reader
    if(!file_exists($file_name)){
        die("File not found!");
    } else{
        $xmlreader->open($file_name);
    }

    while( $xmlreader->read()) 
    {           
            if($xmlreader->nodeType !== XMLReader::ELEMENT){
                continue;
            }
            if($xmlreader->localName === 'reading')
            {
                $time = str_replace(':' , '', $xmlreader->getAttribute('time'));  //get time and replace : to no space
                $date = str_replace('/' , '', $xmlreader->getAttribute('date'));  //get date and replace / to no space
                $date = substr($date, -4);

                if ($time == '080000' && $date == $dropYearVal) //when time and year is matched
                {
                    $row['date'] = $xmlreader->getAttribute('date');    //store date into array
                    $row['no2'] = $xmlreader->getAttribute('val');      //store val into array
                    $records[] = $row;                                  //store both arrays into array
                    $found = true;                                      //when is found then it's ture 
                }
            }      
    }
    

// Sorting array by time (ASC ORDER)
//usort($records, function($firstItem, $secondItem) {
//        $timeStamp1 = strtotime($firstItem['date']);
//        $timeStamp2 = strtotime($secondItem['date']);
//        return $timeStamp1 - $timeStamp2;
//    });
//    var_dump($records);

    if($found == false) //when it's false then means the time and year is not match
    {
        echo "Try different year. No readings in this year of ".$dropYearVal;
    }

?>

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Dates', 'NO2'],
            <?php         
                foreach ($records as $reading) 
                {
                    echo "['" . $reading['date'] . "'," . $reading['no2'] . ",],";
                }
                ?> 

        ]);

        var options = {
          title: 'Dates vs. NO2 Concentration',
          hAxis: {title: 'Dates', minValue: 0, maxValue: 365},
          vAxis: {title: 'NO2 Concentration', minValue: 0, maxValue: 15},
          legend: 'none'
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }

    </script>
  </head>
    
  <body>
    <form>
        <!--Dropdown to select venue-->
        <select name="Dropdown" method="get" onchange='this.form.submit()'>
        <option selected="selected">
        <?php
            echo $dropDownVal;
        ?>    
        </option>
        <?php
        foreach($loc as $value):
        echo '<option value="'.$value.'">'.$value.'</option>'; 
        endforeach;
        ?>
        </select>
        
        <!--Dropdown to select year-->
        <select name="Year" method="get" onchange='this.form.submit()'>
        <option selected="selected">
        <?php
            echo $dropYearVal;
        ?>    
        </option>
        <?php
        foreach($year as $value):
        echo '<option value="'.$value.'">'.$value.'</option>'; 
        endforeach;
        ?>
        </select>
        <noscript><input type="submit" value="Submit"></noscript>
    </form>
      
    <div id="chart_div" style="width: 1000px; height: 500px;"></div>
   </body>
</html>
