# Soo Zheng Shen 14033619#

**URLs:** 

In this assignment, there are two main task that the author require optimised data chunks into smaller pieces from Excel file into 6 XML which make thing much easier and quicker to process which is the Task 1. XMLReader is used to read the file instead of other method. After the 6 XML is generated from the Excel file, the data still need be normalized further as it is still large to be processed. The XMLReader() and XMLWrite()  streaming parsers is used to generate the following normalized XML data.

> File : csv_to_xml.php 

	while (($data = fgetcsv($handle, 200, ",")) !== FALSE) {
        
        if ($data[0] == $key) {
			$rec = '<row count="' . $count . '" id="' . $row . '">';
		
			for ($c=0; $c < $cols; $c++) {
				$rec .= '<' . trim($header[$c]) . ' val="' . trim($data[$c]) . '"/>';
			}
			$rec .= '</row>';
			$count++;
			$out .= $rec;
		}
		$row++;
	}


The Task 2  is to visualize the processed data that has normalized using the streaming parsers into two charts which is the scatter chart and line graph. Each chats give an accurate rendition of data set an appropriate resolution based o the normalized data.

**Scatter chart** that show a year worth of data from a specific station from a specific station at a certain time of day.


**Line chart** show the levels in any 24 hours period on any date (user selectable) for any of the six station). In order to produce this line chart, it is require to find the average of the reading within an hour.

Both are using XMLreader to read the XML and put it into array.

> File: line_graph.php

    while( $xmlreader->read()) 
    {           
            if($xmlreader->nodeType !== XMLReader::ELEMENT){
                continue;
            }
            if($xmlreader->localName === 'reading')
            {
                $time = str_replace(':' , '', $xmlreader->getAttribute('time'));
                $date = str_replace('/' , '', $xmlreader->getAttribute('date'));
                $date = substr($date, -4);

                //if ($time >= '080000' && $time <= '085900' && $date == $dropYearVal)
                if ($time == '080000' && $date == $dropYearVal)
                {
                    $row['date'] = $xmlreader->getAttribute('date');
                    $row['no2'] = $xmlreader->getAttribute('val');
                    $records[] = $row;
                    $found = true;
                }
            }      
    }


Google Chert is the script that used to generate the charts using the example code give.

	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
       <script type="text/javascript"> 
      google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'X');
          data.addColumn('number', '<?php echo $selectedLoc." on ". $selectDate ;?>');
            
            
          data.addRows([
            <?php 
            for($hour=0;$hour<24;$hour++){

                $no2ByHour[$hour] = countAverage($records, $hour);
            } 
            ?>
          ]);

          var options = {
            title: ' NO2 Value of <?php echo $selectedLoc; ?> on <?php echo $selectDate; ?> ',  
            hAxis: {title: 'Hours' ,minValue:0,maxValue:24},
            vAxis: {title: 'NO2 Concentration'},
                pointSize: 3,
                pointShape: 'circle'
            };

          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

          chart.draw(data, options);
        }
       </script>


**SimpleXML**

•	Easy object-oriented tree-based XML parsing

•	Allow viewing XML as tree of objects

•	Accessing child elements using elements name as 
property of object

**DOM**

•	Object-oriented tree-based XML navigation, creation and modification

•	Large and complex API

•	Allow access for all node types, create and modify complex documents

•	Advance navigation and functionality

**XMLReader**

•	Stream-oriented, forward only XML parsing

•	Do not load the entire document into memory

•	Only allow small pieces of document to be processed

•	Push parser via xml extension and pull parser via 

•	Do not allow editing and almost no navigation  


The reason for using the XMLReader extension is because it is an XML Pull parser. It is like a cursor for the reader because it is going forward on the document stream and stopping at each node on the way. Simple cannot read large files while parsing huge xml file is not a problem for using streaming parsers so it will be used only when reading large files. The reason is it do not load the entire document into memory which simplexml will load it entirely into the memory caused it overloaded.


The charting and data visualisation can be extended by adding an option allow the user to select the chart that they desired such as bar charts, calender charts and so on. New features can be added into the current chart for both line graph such as allow the user to search specific 


## Reference ##
[https://www.sitepoint.com/parsing-xml-with-simplexml/]((https://www.sitepoint.com/parsing-xml-with-simplexml/))

[http://php.net/manual/en/class.xmlreader.php](http://php.net/manual/en/class.xmlreader.php)

[https://www.ibm.com/developerworks/library/x-pullparsingphp/](https://www.ibm.com/developerworks/library/x-pullparsingphp/)

[https://rythie.com/blog/blog/2011/02/27/using-a-hybrid-of-xmlreader-and-simplexml/](https://rythie.com/blog/blog/2011/02/27/using-a-hybrid-of-xmlreader-and-simplexml/)